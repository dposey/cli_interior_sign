# CLI_Interior_Sign

A fun design challenge

This code is in response to the requirement in /doc/CLI_Interior_Sign.pdf. The idea is
to create a simulation of a Sign typically seen on a bus or other mass-transit vehicle
that can display banner text. Then simulate a vehicle moving between stops on a route.
The set of banner symbols is provided in /doc/letters.txt (which is described in the
requirements pdf). Stops themselves are in /doc/stops.json and are obviously in JSON
format. Location data is provided (in /doc/locations.csv). A quick inspection of this
data and a little typing in google.maps shows the data to be a sequence of GPS samples
from the Green Bay WI, metropolitan area. The display is limited in size initially to
7x128 pixels. As the vehicle moves along the route, the Bus simulation must determine
when it has reached a stop, by calculating the distance from it's location to the set
of stops, and when that distance falls below a minimum radius defined for a stop,
the bus has arrived there. While the bus is in the stop it should display the friendly
name of the stop (also in stops.json). As the Bus departs the stop, it should display
the next stop in the route. Then as it travels (outside any stops) it should alternate
between displaying the time (from the GPS data) and the route name.

There are a number of optional features, including larger displays and even a version
of the classic arcade game "Pong". That was frankly outside the time budget available
for the project. Option #1 however, I felt like was essential because most, if not
all of the messages that need to be displayed on the Sign would not fit on a 7x128
pixel screen. If the screen cannot be scrolled, the Sign would be of limited utility.
Option #2 for a larger, two line display was a stretch goal I felt was possible, but
I left it to last, mostly because how it was to be used was not well defined in the
requirements. For instance, whould two lines be treated as separate displays? Or
should one line wrap to the other? Either would require a different approach. However,
I will ask for additional time (which was offered as a possibility) and if available,
I believe this would be doable.

I ended up decomposing the problem into three domains: Sign, Bus and GPS. I decided
to just build each into a separate executable, both for convenience and utility. I
chose C++ on Linux as that is what I'm most familiar with and from talking to
the "customer" they are clearly Linux people.  I don't know Windows well enough for
this. Plus honestly, Windows just isn't used in Embedded Systems. I also wanted to
keep this as simple as possible without any exotic dependencies. Unfortunately, since
I chose to use dbus for inter-process communications and remote procedure calls, I
couldn't get by without a C++ binding for it. Since I have used dbus-cxx in the past
and understand it reasonably well, I chose that and it is normally not included in
standard Linux distributions. I have a receipt for it in Yocto (it isn't even a
standard package there) but I know of now pre-built package for Debian-based distos,
so I will have to ask them to compile it from scratch or maybe just link to it's
static library. I normally link to the shared lib. There are a couple of additional
dependancies for libdbus-cxx, but they are commonly found on Linux distros. Another
option might be to build it into a container. I did a Docker environment for Penn
State awhile back, but I'll need to brush up on that one.

The Sign simulation exposes a simple API over dbus using libdbus-cxx Object Proxies.
These let the user treat the Sign much like any other native object. For example
the user can display a message on the Sign by calling a method on an instance of
the Sign's Object Proxy which the Bus has instantiated. The underlying dbus dialog
handled by the library. Message text is converted into an array of strings which
represent the rows of pixels on the display using the set of symbols provided in
/doc/letters.txt. To paint the display, the Sign simply takes the first 128
characters in each row and writes them to stdout in sequence, each line followed
by a CR/LF. The display can be scrolled by simply tracking what character is on the
extreme left of the display and recalculating what character in the strings of
the framebuffer array to start with each to the screen is repainted.  This is
complicated somewhat because the banner symbol representations of each character
provided in /doc/letters.txt is of variable width. As a result there is actually
a Chracter Cursor and a Pixel Cursor used in this process. The API is provides
the following methods: msg, scroll, erase and blank.

The GPS simulation is actually taken from code I have written before with slight
modifications to use the location data provided. It mimics the dbus interface that
was introduced to 'gpsd' some time ago. It essentially sends data from a GPS fix
via a dbus signal.  In the actual gpsd code, they send a unstructured set of native
datatypes as signal payload.  I didn't like that as it makes using the dbus payload
tedious. I created a custom data type for it so one can access the data from the
fix as a structure. The simulation simply parses the locations into a deque from
the STL and then infinitely iterates over it, signaling each fix over the bus.
The loop time is 1 second, however, in order to be as accurate as possible to the
location data, it calulates the time (in seconds) between the present sample and
the next one in the deque and sleeps that amount of time between signals. As each
fix is signalled, from the front of the deque, it is re-inserted into the back.
That way the data loops forever.

The Bus reads in /doc/stops.json using a header-only software for the purpose I
found on the 'net.  It is really well documented and easy to use.  I never use
json directly and I got working pretty quickly which is a testament to the
designer's skill and documentation! See my notes in /doc/blog.txt for attribution.
The Bus then listens to dbus for the fix signals from GPS (FIXDATA) and as each
fix is received, it calculates the distance to each stop using the haversine
formula mentioned in the requirements doc. Again this was something I had never
used as I have primarily been interested in reading and recording/serving the
data in the past, not using it. Yet again, google is useful and I was able to find
public domain code quickly. This code is attricuted in the source file bus.cpp.
As each distance is computed, it is compared to the fence radius for every stop.
If the distance is inside *any* stop radius, the bus is considered to have arrived
at that stop. It then displays the Stop's name. When the Bus leaves the radius,
the next stop in the route is displayed. Once all characters in the next stops
name have been displayed, the bus alternates between the current time (from GPS
fixes) and the route name.

