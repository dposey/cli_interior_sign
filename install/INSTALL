   INSTALLING
   
   Requirements for installing the simulation:
   
   1) Basic knowledge of the Linux Command Line.
   2) A 64-bit machine (a VM is suitable) with a Debian-based Linux distro.
   3) A way to get the package/tar files on that machine.
   4) Root priviledges.
   
   Note: I recommend using Ubuntu for this or one of it repackaged derivitives.
         It is just easiler and I provide a VirtualBox VM already set up with
         Xubuntu which is my prefered disto these days. See below.
   
   Step 1: Download the two required files from the repo here:
   
   https://gitlab.com/dposey/cli_interior_sign/install
   
   The two files needed are libdbus-cxx_0.10.0_x86_64.deb and dallas.sign.tgz
   
   Step 2: Install the libdbus-cxx package with dpkg and ldconfig:
   
   $ sudo dpkg --install libdbus-cxx_0.10.0_x86_64.deb
   $ sudo ldconfig
   
   Step 3: Install the sign simulator using tar. I suggest somewhere in your
           home directory that you can easily remove later.
           
   $ tar xf dallas.sign.tgz
   
   Which should leave you with a single subdir named "bin". All the files
   required for the simulation are in there:
   
   $ ls bin
   bus  GPS  letters.txt  locations.csv  sign  stops.json
   
   
   The simulation consists of 3 executables: sign, bus and GPS. Each should be
   invoked in a separate terminal window.
   
   Run the sign first:
   
   $ cd bin
   $ ./sign
   
   You can force it to the background if you like but there is no need. Adjust
   your window size to a minimum of 8 rows and at least 128 characters.
   
   Next invoke the bus:
   
   $ ./bus
   
   The bus will idle until it begins receiving GPS fix data over dbus so nothing
   will be displayed. However, once you invoke the GPS program, it will begin
   displaying the bus position, and the distance to each of the stops in the
   route. It refreshs this information as it processes each fix it receives.
   It also some other information that is useful for underdtanding what the
   code is up to. When the bus arrives withing the radius specified for any
   stop, the bus is assumed to have arrived and it will display an indicator
   on it's console output.  More importantly, it will display the Stop's name
   on the sign. Once the bus leaves the stop, it will display the next Stop's
   name (at least one time) and then begin alternating between the Route ID and
   the current time (taken from the current GPS fix). The bus will run this
   route ad infinitum.
   
   The last step is to invoke the GPS simulator (again in seperate terminal):
   
   $ ./GPS
   
   The program will begin to signal each gps fix in the locations database over
   dbus in order. When it reaches the last fix, it will loop back to the
   beginning and repeat the cycle until halted. The cycle time is significantly
   higher than the 1 second suggested in the requirements document for the
   the simple reason that it takes *forever* for the bus to complete the
   route in that case and in fact the GPS data itself was sampled at a rate of 
   30 hz which is even slower. The GPS program does not write any useful data
   to it's console. However you can observe it's work using dbus-monitor with
   this command (in another terminat is best):
   
   $ dbus-monitor --session member="FIXDATA"
   
   It is also useful to watch the RPC interaction over dbus between the Bus
   and the Sign like this:
   
   $ dbus-monitor --session interface="Sign.Control"
   
   That's about it. You can of course invoke the sign program and pass it a 
   string which it will display and exit. That is in the requirements:
   
   $ ./sign "TESTING!"
   
   I have included a screenshot of how I arranged my terminal windows for
   testing that illustrate what I was trying to convey above.  See sign.png in 
   the install directory. In addition, I have included a VirtualBox VM setup
   for testing. You will find it in the install directory as well: testSign.ova.
   You should be able to import it into VirtualBox 6.1 and logon under my
   username (dallas) with password 'transloc'
   
   If you guys have trouble installing or running the simulation, feel free
   to contact me at:
   
   xthunderheartx@gmail.com
   
   Thx - Dallas Posey
   
   