//==============================================================================
//                                  GPS
//------------------------------------------------------------------------------
//@file gps.cpp
//------------------------------------------------------------------------------
//@brief A simple gpsd simulator (only the dbus interface is supported)
//==============================================================================

// Linux headers ...
#include <stdio.h>
#include <unistd.h>
#include <syslog.h>

// C/C++ Headers ...
#include <cstring>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>

// DBus headers ... the order is important!
#include <gpsFix.h>
#include <dbus-cxx.h>

// Application headers ...
#include "gps.h"

DBus::Connection::pointer             connection;

using namespace std;

//------------------------------------------------------------------------------
// initApp: Do any initialization we need
//------------------------------------------------------------------------------

void initApp ()
{
  // Setup syslog
  setlogmask(LOG_UPTO(LOG_DEBUG));
  openlog (LOGSRC, LOGOPTIONS, LOGFACILITY);
  syslog (LOG_INFO, "GPS simulation started ...");
}


//------------------------------------------------------------------------------
// fixMissingData: strtok has a bad habit of not handling consecutive tokens
//                 with no intervening data. Unfortunately, heading data in the
//                 locations CSV file is missing from some samples. If we just
//                 stick a space "0" in there, strtok will work. This has the
//                 unfortunate consequence of causing the vehicle to appear to
//                 yaw wildly sometimes (LOL!), but hey ... it ain't Mars Rovers
//                 folks, it's just a demonstration.
//------------------------------------------------------------------------------

void fixMissingData(string *asciiFix)
{
  size_t pos = 0;
  
  while(pos != string::npos)
  {
    pos = asciiFix->find(",,");
    if (pos != string::npos) asciiFix->replace(pos, 2, ",0,");
  }
}


//------------------------------------------------------------------------------
// convert2Epoch: The data in the GPS samples is a string rep of broken down
//                time. However, gpsd signals seconds since Epoch 1 (01/01/1970)
//                because that is what it gets from the Sky. So convert to
//                epoch seconds for the trip across dbus. Note that the
//                consumer of the data (the Bus) reverses this process for
//                display on the sign.
//------------------------------------------------------------------------------

double convert2Epoch(char *asciiTime)
{
  struct tm t;
  double secondsSinceEpoch = 0;
  
  strptime(asciiTime, "%Y-%m-%dT%H:%M:%S", &t);
  secondsSinceEpoch = mktime(&t);

  return secondsSinceEpoch;
}


//------------------------------------------------------------------------------
// readLocations: Read the locations file into a deque of gpsFix_t. This is
//                our route and we could traverse it forwards or backwards.
//------------------------------------------------------------------------------

uint8_t readLocations(std::deque<gpsFix_t>& gpsLocations)
{
  string    asciiFix;
  gpsFix_t  gpsFix;
  char      charFix[512];
  
  // Open locations.csv
  ifstream locationsStream (LOCATIONS_PATH, ifstream::in);
  
  // Make sure we found it and read 'em in
  if (!locationsStream)
  {
    syslog(LOG_INFO, "OOPS! Couldn't open locations.csv!");
    return (-1);
  }
  
  // Discard the first line ... it's cruft
  std::getline(locationsStream, asciiFix);
  
  // Now read each line and parse it into a gpsFix_t suitable for signalling
  // over dbus

  while(std::getline(locationsStream, asciiFix))
  {
    // Unfortunately, some of the fix data is some of the fields, so we
    // get 2 delimiters with no data between which honks strtok right
    // up. One easy fix would be to inject a "0" between any empty fields
    
    fixMissingData(&asciiFix);
    
    strcpy(charFix, asciiFix.c_str());
    gpsFix.latitude = stod(strtok(charFix, ","));
    gpsFix.longitude = stod(strtok(nullptr, ","));
    gpsFix.course = stod(strtok(nullptr, ","));
    gpsFix.speed = stod(strtok(nullptr, ","));
    char *tmp = strtok(nullptr, ",");
    gpsFix.time = convert2Epoch(tmp);;
    
    // Stick -1 in the data we don't need
    gpsFix.mode = -1;
    gpsFix.TDOP = -1;
    gpsFix.HDOP = -1;
    gpsFix.altitude = -1;
    gpsFix.ADOP = -1;
    gpsFix.CDOP = -1;
    gpsFix.SDOP = -1;
    gpsFix.climb = -1;
    gpsFix.CLDOP = -1;
    gpsFix.deviceName = "FAKEGPS!";
    
    gpsLocations.push_back(gpsFix);
  }
  
  return(0);
}


//------------------------------------------------------------------------------
// term: handle sigterm shutdown signal from os
//------------------------------------------------------------------------------

void term(int signum)
{
  // Release the dbus name and take the pipe.
  connection->release_name("com.dallas.gps");
  syslog (LOG_INFO, "GPS shutting down.");
  exit(0);
}


//------------------------------------------------------------------------------
// setupSigtermhandler: Register a handler for common termination signals
//------------------------------------------------------------------------------

void setupSigtermhandler()
{
    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = term;
    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGHUP, &action, NULL);
}


//------------------------------------------------------------------------------
// Start here ...
//------------------------------------------------------------------------------

int main()
{
  initApp();
  gpsFix_t        sigvalue;
  deque<gpsFix_t> locations;  

  // Init libdbus-cxx and create a connection to SESSION BUS
  DBus::init();
  connection = DBus::Connection::create(DBus::BUS_SESSION);
  
  // Request a name on the bus
  int ret = connection->request_name("com.dallas.gps", DBUS_NAME_FLAG_REPLACE_EXISTING);
  if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret) return -1;
  
  // Now create a signal to yakk channel updates with ...
  DBus::signal<void, gpsFix_t>::pointer signal =
    connection->create_signal<void, gpsFix_t>("/com/dallas/GPS","com.dallas.gps", "FIXDATA");

  // Read in the locations if possible, bail otherwise
  if (readLocations(locations) < 0)
  {
    syslog(LOG_ERR, "Can't read locations");
    exit (-1);
  }  

  // Loop through our trip until DoomsDay ...
  for(int i=0;;i++)
  { 
    // Get a reference to the top of the deque
    gpsFix_t fix = locations.front();
   
    // Signal from the front of the deque
    signal->emit(fix);
    connection->flush();
    
    // Pop it off the top of the deque 
    locations.pop_front();    
    // Stick it back at the bottom.
    locations.push_back(fix);
    
    // Try to simulate real-time. Note that if the fix time is greater than
    // than the time on the next location, we are are about to loop back to
    // the beginning of the drive. We don't want to cause any rifts in the
    // space-time continuum (or wait a gazillion seconds for the next sample!)
    // so just sleep for 2 seconds. A bit artificial perhaps but it is a
    // simulation after all. The whole thing is artificial. Note that the front 
    // of the deque is the *next* location's fix.
    
    if (locations.front().time >= fix.time)
    {
      usleep(250000 *(locations.front().time - fix.time));
    }
    else usleep (250000);
  }
}
