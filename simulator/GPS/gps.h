//==============================================================================
//------------------------------------------------------------------------------
//@file gps.h
//------------------------------------------------------------------------------
//@brief Header file with GPS specific definitions.
//==============================================================================

#pragma once

#define LOGSRC        "GPS"               // Who's loggin'
#define LOGOPTIONS     LOG_CONS | LOG_PID	// How we log
#define LOGFACILITY		 LOG_USER           // Our 'hood

#define LOCATIONS_PATH "./locations.csv"
