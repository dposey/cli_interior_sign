//==============================================================================
//                                gpsFix.cpp
//------------------------------------------------------------------------------
//@file gpsFix.cpp
//------------------------------------------------------------------------------
//@brief Definitions used in communicating GPS fix data on dbus
//==============================================================================

#include <gpsFix.h>
#include <dbus-cxx/enums.h>

DBus::MessageIterator &operator>>(DBus::MessageIterator& i, gpsFix_t& f)
{
  DBus::MessageIterator subiter = i.recurse();
  
  subiter >> f.time;
  subiter >> f.mode;
  subiter >> f.TDOP;
  subiter >> f.latitude;
  subiter >> f.longitude;
  subiter >> f.HDOP;
  subiter >> f.altitude;
  subiter >> f.ADOP;
  subiter >> f.course;
  subiter >> f.CDOP;
  subiter >> f.speed;
  subiter >> f.SDOP;
  subiter >> f.climb;
  subiter >> f.CLDOP;
  subiter >> f.deviceName;

  return i;
}

DBus::MessageAppendIterator& operator<<(DBus::MessageAppendIterator& i, const gpsFix_t& f)
{
  i.open_container( DBus::CONTAINER_STRUCT, "" );
  
  i.sub_iterator()->append((double)f.time);
  i.sub_iterator()->append((int32_t)f.mode);
  i.sub_iterator()->append((double)f.TDOP);
  i.sub_iterator()->append((double)f.latitude);
  i.sub_iterator()->append((double)f.longitude);
  i.sub_iterator()->append((double)f.HDOP);
  i.sub_iterator()->append((double)f.altitude);
  i.sub_iterator()->append((double)f.ADOP);
  i.sub_iterator()->append((double)f.course);
  i.sub_iterator()->append((double)f.CDOP);
  i.sub_iterator()->append((double)f.speed);
  i.sub_iterator()->append((double)f.SDOP);
  i.sub_iterator()->append((double)f.climb);
  i.sub_iterator()->append((double)f.CLDOP);
  i.sub_iterator()->append((std::string)f.deviceName);
  
  i.close_container();
  return i;
}
