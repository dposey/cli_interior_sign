//==============================================================================
//                     Copyright 2010 Noregon Systems, Inc.
//                             All rights reserved
//
// The information contained herein is proprietary data and is not for
// dissemination or disclosure, in whole or in part, for any purpose except as
// authorized in writing by Noregon System, Inc.
//------------------------------------------------------------------------------
//@file gpsFix.h
//------------------------------------------------------------------------------
//@brief Declarations used in communicating GPS fix data on dbus
//==============================================================================

#ifndef GPSFIX_H
#define GPSFIX_H

#include <string>
#include <vector>

//------------------------------------------------------------------------------
// The gps daemon (gpsd) broadcasts fix data on dbus using the format declared
// in the structure definition below. The dbus spec defines a number of
// different native types that can be aggregated into a structure like this one.
// The DBUS_SIGNATURE describes the individual members of the struct and is used
// by dbus-cxx to parse the custom structure for it's own use. See the following
// URL for example code:
//
// https://gitlab.com/dposey/dbus-cxx/tree/master/examples/basics/types
//
// also this thread on the dbus-cxx google group:
//
// https://groups.google.com/forum/#!topic/dbus-cxx/AxLXh8AyIj0
// 
// The dbus specification is extremely useful in understanding how data is
// marshalled. Search the page for "Type System":
// 
// https://dbus.freedesktop.org/doc/dbus-specification.html
//------------------------------------------------------------------------------

typedef struct _gpsFix
{
  static constexpr const char* DBUS_SIGNATURE = "(didddddddddddds)";
  double    time;
  int32_t   mode;
  double    TDOP;
  double    latitude;
  double    longitude;
  double    HDOP;
  double    altitude;
  double    ADOP;
  double    course;
  double    CDOP;
  double    speed;
  double    SDOP;
  double    climb;
  double    CLDOP;
  std::string    deviceName;
} gpsFix_t;

//------------------------------------------------------------------------------
// Return dbus custom data description. This has to live in the DBus namespace
// in order to work with libdbus-cxx.
//------------------------------------------------------------------------------

namespace DBus
{
  inline std::string signature(gpsFix_t) {return gpsFix_t::DBUS_SIGNATURE;}
}

// The placement of this include is weird but critical. Don't move it. Trust me
// the preprocessor will barf all over the place if you do.
 
#include <dbus-cxx.h>

//------------------------------------------------------------------------------
// These iterators insert/extract a gpsFix struct into/from a DBus::Message.
//------------------------------------------------------------------------------

DBus::MessageIterator &operator>>(DBus::MessageIterator& i, gpsFix_t& f);
DBus::MessageAppendIterator& operator<<(DBus::MessageAppendIterator& i, const gpsFix_t& f);

#endif // GPSFIX_H
