/**
//==============================================================================
//                                   Bus
//------------------------------------------------------------------------------
// @file bus.cpp
//------------------------------------------------------------------------------
// @brief A simple Bus simulation
//==============================================================================
*/

// Linux headers ...
#include <stdio.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>

// C/C++ Headers ...
#include <cstring>
#include <cmath> 
#include <iostream>
#include <fstream>
#include <iomanip>
#include <exception>

#include "gpsFix.h"
#include <dbus-cxx.h>

// Application headers ...
#include "bus.h"
#include "json.hpp"
#include "stop.hpp"
#include "sign.h"

using namespace std;
using namespace nlohmann;
using namespace BusStop;

DBus::Connection::pointer connection;
DBus::Dispatcher::pointer dispatcher;

SignProxy::pointer ourSign;
DBus::signal_proxy<void, gpsFix_t>::pointer gpsFix;

string        ourRoute = "ROUTE: ";
vector<Stop*> busRoute;

//------------------------------------------------------------------------------
// haversine: Caculate distance (in kilometers) between locations.
//------------------------------------------------------------------------------
// C++ program for the haversine formula 
// 
// This code is contributed by Mahadev.
// 
// (see https://www.geeksforgeeks.org/haversine-formula-to-find-distance- 
//  between-two-points-on-a-sphere/)
//------------------------------------------------------------------------------

static double haversine(double lat1, double lon1, double lat2, double lon2) 
{ 
  // distance between latitudes and longitudes 
  double dLat = (lat2 - lat1) * M_PI / 180.0; 
  double dLon = (lon2 - lon1) * M_PI / 180.0; 

  // convert to radians 
  lat1 = (lat1) * M_PI / 180.0; 
  lat2 = (lat2) * M_PI / 180.0; 

  // apply formulae 
  double a = pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2); 
  double rad = 6371; 
  double c = 2 * asin(sqrt(a)); 
  return rad * c; 
}


//------------------------------------------------------------------------------
// checkStops: Calculate the distance from our current fix to each stop on
//             the route and handle display.
//------------------------------------------------------------------------------

void checkStops(gpsFix_t fix)
{
  bool                    atStop = false;
  char                    msgBuff[128];
  struct tm              *t;
  const time_t            fixTime = fix.time;
  static string           nextStopName = "WELCOME!";
  size_t                  remainder;
  vector<Stop*>::iterator nextStop;
  
  cout << "Bus state: " << state << endl << endl;
  
  // Get the time from the fix.
  t = gmtime(&fixTime);
  strftime (msgBuff,sizeof(msgBuff),"%I:%M:%S%p",t);
  string stringTime(msgBuff);
  
  // Iterate over the stops and calculate the distance from the present fix
  // to each one.
  for(vector<Stop*>::iterator it = busRoute.begin(); it != busRoute.end(); ++it)
  {
    double distance = haversine((*it)->GetLat(), (*it)->GetLon(),
                                fix.latitude, fix.longitude);
                                
    distance *= 1000; // Convert to meters

    cout << "State: " << state << " Distance to stop: " << (*it)->GetStopId()
         << " is " << distance;

    // Are we inside a Stop's Radius?
    if (distance <= (*it)->GetRadius())
    {
      // Yep.
      cout << " <-- Inside radius" << endl;
      atStop = true;
      
      switch (state)
      {
        // Just arrived. Display Stop's name
        case TRIP:
        case TIME:
        ourSign->erase();
        ourSign->msg ((*it)->GetStopName());
        
        // Get ready for departure
        if ((*it)->IsLastStop())  // Last Stop?
        {
          nextStop = it;          // Yep: There is no next
          nextStopName = "END OF LINE!";
        }
        else 
        {
          nextStop = it;
          advance(nextStop, 1);   // Nope: next
          nextStopName = "NEXT: " + (*nextStop)->GetStopName();
        }
        
        state = INSIDE;  // Transition
        break;

        // Scroll Stop's name while at the stop
        case INSIDE:
        ourSign->scroll(4);
        break;
        
        default:
        break;
      }      
    }
    else
    {
      cout << endl;
    }
  }
  
  cout << endl << "State: " << state << " atStop: " << atStop << endl;

  switch (state)
  {
    case INSIDE:
    if ((!atStop)) state = LEAVING;  // Transition if not within radius
    break;
    
    // Leaving the Stop. Display next Stop's name
    case LEAVING: 
    ourSign->erase();
    ourSign->msg (nextStopName);
    state = SCROLLNEXT;
    break;

    // Scroll until all of next Stop's name has been displayed.
    case SCROLLNEXT:
    remainder = ourSign->scroll(4);
    if (!(nextStopName.size() - 1 > remainder))
    {
      state = TRIP;       // Transition
      ourSign->erase();
    }
    break;
    
    case TRIP:
    ourSign->erase();
    ourSign->msg (ourRoute);
    state = TIME;         // Transition (toggle time)
    break;

    case TIME:
    ourSign->erase();
    ourSign->msg(stringTime);
    state = TRIP;         // Transition (toggle route) 
    break;
    
    default:
    break;
  }
}


//------------------------------------------------------------------------------
// HandleResult: Signal handler that runs on dispatcher's thread. Handle
//               position fixes from GPS
//------------------------------------------------------------------------------

void handleFix(gpsFix_t fix)
{
  
  cout << "Current position:" << endl << endl;
  cout << "Longitude: " << fix.longitude << endl;
  cout << "Latitude : " << fix.latitude << endl;
  cout << "Course   : " << fix.course << endl;
  cout << "Speed    : " << fix.speed << endl;
  cout << "Time     : " << (long long)fix.time << endl << endl;

  // Better keep an eye on this process
  try
  {
    checkStops(fix);
  }
  catch(std::exception const& e)
  {
    std::cerr << "Exception while checking Stops: " << e.what() << endl;
  }
}

//------------------------------------------------------------------------------
// initApp
//------------------------------------------------------------------------------

void initApp()
{
  // Setup syslog
  setlogmask(LOG_UPTO(LOG_DEBUG));
  openlog (LOGSRC, LOGOPTIONS, LOGFACILITY);
  syslog (LOG_INFO, "Bus started ... rhooom-rhooom :)");
}


//------------------------------------------------------------------------------
// initDbus
//------------------------------------------------------------------------------

void initDbus()
{
  // Init libdbus-cxx
  DBus::init();
  
  // Create a dispatcher and a connection
  dispatcher = DBus::Dispatcher::create();
  connection = dispatcher->create_connection(DBus::BUS_SESSION);
  
  // Create a SignProxy object (an 'object_proxy') on the connection
  ourSign = SignProxy::create(connection);
  
  // Create a signal proxy for the VID response message
  gpsFix = connection->create_signal_proxy<void, gpsFix_t>("/com/dallas/GPS", \
           "com.dallas.gps", "FIXDATA");
  
  // Connect a signal handler to the proxy
  gpsFix->connect(sigc::ptr_fun(handleFix));

  syslog(LOG_INFO, "Added a Sign to our Bus");
}


//------------------------------------------------------------------------------
// readStops: Create our route from stops.json.
//------------------------------------------------------------------------------

uint32_t readStops(vector<Stop*> &route)
{
  uint32_t result = 0;  // Let's be positive
    
  ifstream stopsStream (STOPS_PATH, ifstream::in);
  json stopsJSON;

  // Make sure we found stops.json and read 'em in
  if (stopsStream)
  {
    stopsStream >> stopsJSON;
  }
  else
  {
    syslog(LOG_INFO, "OOPS! Couldn't open stops.json!");
    return (-1);
  }
  
  // Get the Route ID
  ourRoute += to_string(stopsJSON["tripId"]);

  // Convert them into instances of Stop and construct a route.
  for (json::iterator it = stopsJSON["stops"].begin();it != stopsJSON["stops"].end();it++)
  {
    json currentStopJSON = *it;
    Stop* currentStop = new Stop;
    *currentStop = currentStopJSON.get<BusStop::Stop>();
    currentStop->SetLastStop(false);
    route.push_back(currentStop);
  }
  
  // Mark the last stop.
  route.back()->SetLastStop(true);
  
  return result;
}

//------------------------------------------------------------------------------
// Start here
//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  // Setup first
  initApp();

  // Get our route
  readStops(busRoute);
  
  // Connect to dbus
  initDbus();

  // Start with a Clean Sign
  ourSign->erase();
  // Wait for The Trumpet to blow ...
  for(;;){}

  return -1; // We never get here.
}