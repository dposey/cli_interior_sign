/**
//==============================================================================
//                                 bus.h  
//------------------------------------------------------------------------------
// @file bus.h
//------------------------------------------------------------------------------
// @brief Header file with bus specific definitions
//==============================================================================
*/

#pragma once

#define SUCCESS (0)

#ifndef LOGSRC
#define LOGSRC          "BUS"               // Who's loggin'
#endif

#define LOGOPTIONS      LOG_CONS | LOG_PID  // How we log
#define LOGFACILITY     LOG_USER            // Our 'hood

#define STOPS_PATH      "./stops.json"

enum {TRIP, TIME, INSIDE, LEAVING, SCROLLNEXT, NEXTSEGMENT} typedef state_e;
    
state_e      state;
    