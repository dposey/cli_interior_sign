/**
//==============================================================================
//                                 stop.cpp  
//------------------------------------------------------------------------------
// @file stop.cpp
//------------------------------------------------------------------------------
// @brief This file contains the implementation of class Stop.  It doesn't do
//        much at the moment ...
//==============================================================================
*/

#include <syslog.h>
#include "json.hpp"
#include "stop.hpp"

using namespace std;
using BusStop::Stop;

Stop::Stop()
{
  syslog(LOG_INFO, "Creating instance of class Stop");
}

Stop::~Stop()
{
  syslog(LOG_INFO, "Destroying instance of class Stop");
}
