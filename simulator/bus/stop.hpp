/**
//==============================================================================
//                                 stop.hpp
//------------------------------------------------------------------------------
// @file stop.hpp
//------------------------------------------------------------------------------
// @brief Header file for use with class Stop
//==============================================================================
*/

#pragma once

#include <stdint.h>
#include <string>
#include "json.hpp"

// Example of a stop:
//"stop_id": 573,
//"lat": 44.448725,
//"lon": -88.059148,
//"radius": 39.9288,
//"stop_name": "WISCONSIN an GEORGE"

using namespace std;

namespace BusStop       // Need a namespace for json.h type conversions
{
class Stop
{
private:
  bool         lastStop;
  double       lat;
  double       lon;
  double       radius;
  string       stop_name;
  uint16_t     stop_id;

public:

  enum {TRIP, TIME, INSIDE, LEAVING} typedef state_e;

  state_e      state;

  Stop();
  ~Stop();

  Stop& SetLastStop(bool lastStop)
  {
    this->lastStop = lastStop;
    return *this;
  }
  bool IsLastStop() const
  {
    return lastStop;
  }

  Stop& SetState(const state_e& state)
  {
    this->state = state;
    return *this;
  }

  const state_e& GetState() const
  {
    return state;
  }

  Stop& SetLat(double lat)
  {
    this->lat = lat;
    return *this;
  }
  Stop& SetLon(double lon)
  {
    this->lon = lon;
    return *this;
  }
  Stop& SetRadius(double radius)
  {
    this->radius = radius;
    return *this;
  }
  Stop& SetStopId(const uint16_t& stop_id)
  {
    this->stop_id = stop_id;
    return *this;
  }
  Stop& SetStopName(const string& stop_name)
  {
    this->stop_name = stop_name;
    return *this;
  }
  double GetLat() const
  {
    return lat;
  }
  double GetLon() const
  {
    return lon;
  }
  double GetRadius() const
  {
    return radius;
  }
  const uint16_t& GetStopId() const
  {
    return stop_id;
  }
  const string& GetStopName() const
  {
    return stop_name;
  }

  // This lets us convert json<->Stop
  NLOHMANN_DEFINE_TYPE_INTRUSIVE(Stop, stop_id, lat, lon, radius, stop_name)
};
}
