/**
//==============================================================================
//                               Display
//------------------------------------------------------------------------------
// @file display.cpp
//------------------------------------------------------------------------------
// @brief Implements a simulated display analogous to the small LED/LCD screens
//        often used in embedded systems.
//==============================================================================
*/

#include <syslog.h>
#include <iostream>
#include <algorithm>
#include "display.hpp"

//------------------------------------------------------------------------------
// Constructor / Destructor
//------------------------------------------------------------------------------

Display::Display(std::string msg):m_Message(msg)
{
  syslog(LOG_INFO, "Creating Display instance");
  
  SetCCursor(0); // Start at the display's orgin  
}

Display::~Display(){};

//------------------------------------------------------------------------------
// Update: Paint the message on the Display's screen (the console in this case)
//------------------------------------------------------------------------------

void Display::Update()
{
  syslog (LOG_INFO, "Update");
  
  for (int i = 0; i < PIXEL_HEIGHT; i++)
  {
    std::cout << GetDisplay()[i].substr(GetPCursor(), PIXEL_WIDTH) << std::endl;
  }
}


//------------------------------------------------------------------------------
// Blank: Clear the display
//------------------------------------------------------------------------------

void Display::Blank()
{
  syslog (LOG_INFO, "Blank");
  system("clear");
}


//------------------------------------------------------------------------------
// Blank: Erase the display. This is distinct from "blanking" above.
//------------------------------------------------------------------------------

void Display::Erase()
{
  syslog(LOG_INFO, "Erase");
  Blank();
  SetMessage("");
  Update();
}


//------------------------------------------------------------------------------
// Scroll: Move the cursor 
//------------------------------------------------------------------------------

size_t Display::Scroll(size_t value)
{  
  syslog (LOG_INFO, "Scroll: %lu", value);
  
  if ((GetCCursor() + value) < (GetMessage().length()))
  {
    SetCCursor(GetCCursor() + value);
  }
  else
  {
    SetCCursor(0);
  }
  
  Blank();
  Update();
  
  return (size_t) ((GetMessage().length() - GetCCursor()) - 1);
}

