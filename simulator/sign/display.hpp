/**
//==============================================================================
//                                display.hpp
//------------------------------------------------------------------------------
// @file display.hpp
//------------------------------------------------------------------------------
// @brief Header file containing definitions for class Display
//==============================================================================
*/

#pragma once

#include <syslog.h>

#include <string>
#include <array>
#include <vector>

#include "symbol.hpp"

#define PIXEL_HEIGHT (7)
#define PIXEL_WIDTH  (128)

class Display
{
public:
  Display(std::string msg);
  ~Display();

  Display& SetCCursor(size_t newCursor)
  {
    std::string subString;
    
    // If the new cCursor value is > the current cCursor we are scrolling
    // backwards. Not allowed at present.
    // 
    // TODO: Scrolling ackwards would be a nice feature. Might make Rotating
    //       possible?

    if (newCursor >= this->m_cCursor)
    {
      subString = GetMessage().substr(this->m_cCursor, newCursor - this->m_cCursor);
    }
   
    // Update the character cursor.
    this->m_cCursor = newCursor;
    
    // Reset the pCursor to zero and exit *if* the cCursor is reset
    if (this->m_cCursor == 0)
    {
      SetPCursor(0);
      return *this;
    }

    // Now find the total pixel width of the characters we are scrolling
    // past.  Add that to the pixel cursor.

    std::string::iterator it;
    for (it = subString.begin(); it != subString.end(); it++)
    {
      // Get the position of the symbol
      size_t symbolPos = GetCharRep().find(std::toupper(*it));
      // Adjust the pixel cursor
      SetPCursor(GetPCursor() + GetCharacterSet().at(symbolPos)->GetWidth());
    }
    
    return *this;
  }
  
  size_t GetCCursor() const
  {
    return m_cCursor;
  }
  
  Display& SetPCursor(size_t cursor)
  {
    this->m_pCursor = cursor;
    return *this;
  }
  
  size_t GetPCursor() const
  {
    return m_pCursor;
  }

  Display& SetCharacterSet(const std::vector<Symbol*>& characterSet)
  {
    this->m_characterSet = characterSet;
    return *this;
  }

  const std::vector<Symbol*>& GetCharacterSet() const
  {
    return m_characterSet;
  }

  Display& SetMessage(const std::string& Message)
  {
    this->m_Message = Message;
    std::array<std::string, PIXEL_HEIGHT> screen;

    // Reset the Display    
    SetCCursor(0);
    
    // TODO: Isn't a std::string garanteed to be empty on instantiation?
    for (int i = 0; i < PIXEL_HEIGHT; i++) screen[i].clear();
    
    // Clear the Display's 'screen'. The screen array really acts as the
    // Display's framebuffer (which is now clear).

    SetDisplay(screen);

    // Now iterate over m_message and search m_charRep for each of the
    // characters. When we find one, update the the Display by appending each
    // row of it's sprite to the corresponding row of the screen. If we don't
    // find it, we don't have a symbol for that character in the message so just
    // ignore it. Note that the position of the character in the m_charRep
    // corresponds to the position of the corresponding symbol in m_characterSet.
    //
    // TODO: Should we have a Symbol to represent undisplayable characters?

    std::string::const_iterator it;  // Readability below

    for (it = this->m_Message.begin(); it != this->m_Message.end(); it++) 
    {
      // Convert to uppercase
      // 
      // TODO: This is here because some of the stop names have lowercase
      //       letters, but there are no corresponding symbols in letter.txt.
      //       Will work for the moment, but I should get clarification.
 
      char thisChar = *it;
      thisChar = std::toupper(thisChar);

      // Get the position of the symbol
      size_t symbolPos = GetCharRep().find(thisChar);

      // Skip if not found ... undisplayable
      if (symbolPos == std::string::npos) continue;

      // Append the sprite to the Display
      for (int i = 0; i < PIXEL_HEIGHT; i++)
      {
        screen[i] += GetCharacterSet().at(symbolPos)->GetSprite()[i];
      }
    }

    // Add the screen to the Display. The screen array really acts as the
    // Display's framebuffer.

    SetDisplay(screen);

    return *this;
  }

  const std::string& GetMessage() const
  {
    return m_Message;
  }

  Display& SetCharRep(const std::string& charRep)
  {
    this->m_charRep = charRep;
    return *this;
  }

  const std::string& GetCharRep() const
  {
    return m_charRep;
  }

  Display& SetDisplay(const std::array<std::string, PIXEL_HEIGHT>& display)
  {
    this->m_display = display;
    return *this;
  }

  const std::array<std::string, PIXEL_HEIGHT>& GetDisplay() const
  {
    return m_display;
  }

  void Blank();
  void Update();
  void Erase();
  size_t Scroll(size_t);

private:
  size_t                                  m_cCursor;      // Character cursor
  size_t                                  m_pCursor;      // Pixel cursor
  std::string                             m_Message;      // Message in ASCII
  std::string                             m_charRep;      // ASCII Character set
  std::array<std::string, PIXEL_HEIGHT>   m_display;      // Display memory
  std::vector<Symbol*>                    m_characterSet; // Symbol Character set
};
