/**
//==============================================================================
//                                  Sign
//------------------------------------------------------------------------------
// @file sign.cpp
//------------------------------------------------------------------------------
// @brief A simple Sign simulation
//==============================================================================
*/

// Linux headers ...
#include <stdio.h>
#include <unistd.h>
#include <syslog.h>
#include <time.h>

// C/C++ Headers ...
#include <cstring>
#include <iostream>
#include <fstream> 
#include <vector>
#include <dbus-cxx.h>

using namespace std;

// Application headers ...
#include "sign.h"
#include "display.hpp"
#include "symbol.hpp"

Display                     myDisplay("");
DBus::Dispatcher::pointer   dispatcher;
DBus::Connection::pointer   connection;

//------------------------------------------------------------------------------
// initApp
//------------------------------------------------------------------------------

void initApp()
{
  // Setup syslog
  setlogmask(LOG_UPTO(LOG_DEBUG));
  openlog (LOGSRC, LOGOPTIONS, LOGFACILITY);
  syslog (LOG_INFO, "Sign started ...");
}


//------------------------------------------------------------------------------
// initDbus
//------------------------------------------------------------------------------

void initDbus()
{
  // Initialize the DBus library
  DBus::init();

  // Connect to DBus
  dispatcher = DBus::Dispatcher::create();
  connection = dispatcher->create_connection( DBus::BUS_SESSION );
  return;
}


//------------------------------------------------------------------------------
// readLetters: Create our character set from letters.txt.
//------------------------------------------------------------------------------

string readLetters(vector<Symbol*> *characterSet)
{
  string line;
  string charRep;
  ifstream letters (LETTERS_PATH, ifstream::in);

  if (!letters)
  {
    syslog (LOG_INFO, "Could not open letters.txt!");
    return 0;
  }

  // This is the ASCII characterset on the first line of the file.
  getline(letters, charRep);
  
  // Now each group of 8 lines in the file represents a symbol with the
  // first line being a separator. The next 7 lines in the group are a
  // representation of a line of pixels in the symbol starting with the
  // top line. Therefore, each symbol is 7 pixels "tall". However, the
  // width of the symbols varies, with the widest being the "%" which is 12
  // pixels. We will need the width later.  Also, we want to add an additional
  // column of blank pixels on the right of each symbol for spacing.

  for (size_t i = 0; i < charRep.length(); i++)
  {
    Symbol* currentSymbol= new (Symbol);            // This symbol
    array<std::string, PIXEL_HEIGHT> currentSprite; // This symbol's sprite
    getline(letters, line);
    currentSymbol->SetID(i);                 // TODO: Is this useful?
    currentSymbol->SetASCII(charRep.at(i));  // Definitely need this.
    
    // Read the next PIXEL_HEIGHT rows and save them in the symbols sprite
    for(size_t j = 0; j < PIXEL_HEIGHT; j++)
    {
      getline(letters, line);
      currentSprite[j] = line;
     
      // Append an extra blank pixel to the end of the row, effectively
      // adding a blank column to the sprite so we get spacing between
      // symbols when displaying them next to each other.

      currentSprite[j] += " ";
      
      if (currentSprite[j].length() > currentSymbol->GetWidth())
      {
        currentSymbol->SetWidth(currentSprite[j].length());
      }
    }
    
    currentSymbol->SetSprite(currentSprite);
    characterSet->push_back(currentSymbol);
  }
  
  return charRep;
}

//------------------------------------------------------------------------------
// SignAPI: Provide an API we can expose via dbus.
//------------------------------------------------------------------------------

class SignAPI
{
  public:
  
  // Set the message
  uint8_t msg(std::string message)
  {
    myDisplay.SetMessage(message);
    myDisplay.Blank();
    myDisplay.Update();
    return (0);
  }
  
  // Blank the display
  uint8_t blank()
  {
    myDisplay.Blank();
    return (0);
  }
  
  // Erase the display
  uint8_t erase()
  {
    myDisplay.Erase();
    return (0);
  }
  
  // Scroll the display
  uint16_t scroll(uint16_t value)
  {
    return myDisplay.Scroll(value);
  }
  
  // Update the display
  uint8_t update()
  {
    myDisplay.Update();
    return (0);
  }
};


//------------------------------------------------------------------------------
// exposeSignAPI: Expose the API above via dbus-cxx object proxy.
//------------------------------------------------------------------------------

bool exposeSignAPI()
{
  int ret;
  static SignAPI api;
  
  // Request a name on the bus
  ret = connection->request_name("com.dallas.sign", DBUS_NAME_FLAG_REPLACE_EXISTING);
  if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret) return false;
  
  // Create an object
  static DBus::Object::pointer object = connection->create_object("/com/dallas/sign");
  
  // Create methods on the object. Use sigc::mem_fun as these are member
  // functions of a class
  
  object->create_method<uint8_t,string>("msg", sigc::mem_fun(api, &SignAPI::msg));
  object->create_method<uint8_t>("blank", sigc::mem_fun(api, &SignAPI::blank));
  object->create_method<uint8_t>("erase", sigc::mem_fun(api, &SignAPI::erase));
  object->create_method<size_t,size_t>("scroll", sigc::mem_fun(api, &SignAPI::scroll));
  object->create_method<uint8_t>("update", sigc::mem_fun(api, &SignAPI::update));
  
  return true;
}


//------------------------------------------------------------------------------
// term: handle sigterm shutdown signal from os
//------------------------------------------------------------------------------

void term(int signum)
{
  // Release the dbus name and take the pipe.
  connection->release_name("com.dallas.sign");
  syslog (LOG_INFO, "Sign shutting down.");
  exit(0);
}


//------------------------------------------------------------------------------
// setupSigtermhandler: Register a handler for common termination signals
//------------------------------------------------------------------------------

void setupSigtermhandler()
{
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_handler = term;
  sigaction(SIGTERM, &action, NULL);
  sigaction(SIGINT, &action, NULL);
  sigaction(SIGHUP, &action, NULL);
}


//------------------------------------------------------------------------------
// Start here.
//------------------------------------------------------------------------------

int main(int argc, char **argv)
{  
  string          charRep;
  vector<Symbol*> characterSet;

  // Setup
  initApp();
  initDbus();
  
  // Bail if our interface is already owned
  if (!exposeSignAPI())
  {
    syslog(LOG_ERR, "Only one instance allowed!!");
    return -1;
  }
  
  // Read the characterset
  charRep = readLetters(&characterSet);

  // Bail if we don't have one
  if (charRep.empty())
  {
    syslog(LOG_ERR, "No characterset!");
    return -2;  
  }
  
  // Add the characterSet to our Display
  myDisplay.SetCharacterSet(characterSet);
  // And it's character representation
  myDisplay.SetCharRep(charRep);

  // With no parameters, runs until signaled otherwise
  if (argc == 1)
  {
    myDisplay.Erase();    
    myDisplay.SetMessage("Good Morning!");
    myDisplay.Update();
  
    sleep (2);
  
    myDisplay.Erase();
        
    for (;;)
    {
      sleep(5);
    }
  }
  else
  {
    // Display user's first argument.  All others ignored.
    string userMessage(argv[1]);
    
    myDisplay.Erase();
    myDisplay.SetMessage(userMessage);
    myDisplay.Update();
  }

  return 0;
}
