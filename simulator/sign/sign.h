/**
//==============================================================================
//                                  sign.h
//------------------------------------------------------------------------------
// @file sign.h
//------------------------------------------------------------------------------
// @brief Header file with sign specific definitions
//==============================================================================
*/

#pragma once

#define SUCCESS (0)

#ifndef LOGSRC
#define LOGSRC          "SIGN"              // Who's loggin'
#endif

#define LOGOPTIONS      LOG_CONS | LOG_PID  // How we log
#define LOGFACILITY     LOG_USER            // Our 'hood

#define SIGN_DBUS_NAME  "com.dallas.sign"   // How we expose our API

#define LETTERS_PATH "./letters.txt"


//==============================================================================
// libdbus-cxx proxy objects for interfacing with a Sign over dbus
//==============================================================================

class SignProxy: public DBus::ObjectProxy
{
  protected:
    SignProxy(DBus::Connection::pointer conn):
      DBus::ObjectProxy(conn, "com.dallas.sign", "/com/dallas/sign")
    {
      m_method_msg = this->create_method<uint8_t,string>("Sign.Control", "msg");
      m_method_blank = this->create_method<uint8_t>("Sign.Control", "blank");
      m_method_erase = this->create_method<uint8_t>("Sign.Control", "erase");
      m_method_scroll = this->create_method<size_t,size_t>("Sign.Control", "scroll");
      m_method_update = this->create_method<uint8_t>("Sign.Control", "update");
    }

  public:
    typedef DBusCxxPointer<SignProxy> pointer;
    static pointer create(DBus::Connection::pointer conn)
    {
      return pointer(new SignProxy(conn));
    }
    
    uint8_t msg(string message) {return ((*m_method_msg)(message));}
    uint8_t blank() {return ((*m_method_blank)());}
    uint8_t erase() {return ((*m_method_erase)());}
    size_t scroll(size_t value) {return ((*m_method_scroll)(value));}
    uint8_t update() {return ((*m_method_update)());}
    
  protected:
    
    DBus::MethodProxy<uint8_t,string>::pointer m_method_msg;
    DBus::MethodProxy<uint8_t>::pointer m_method_blank;
    DBus::MethodProxy<uint8_t>::pointer m_method_erase;
    DBus::MethodProxy<size_t,size_t>::pointer m_method_scroll;
    DBus::MethodProxy<uint8_t>::pointer m_method_update;
};
