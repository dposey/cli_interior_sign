/**
//==============================================================================
//                                 symbol.cpp  
//------------------------------------------------------------------------------
// @file symbol.cpp
//------------------------------------------------------------------------------
// @brief This file contains the implementation of class Symbol.  It doesn't do
//        much at the moment ...
//==============================================================================
*/

#include <syslog.h>
#include "symbol.hpp"

Symbol::Symbol()
{
  this->m_width = 0;
  syslog(LOG_INFO, "Creating Symbol instance");
}

Symbol::~Symbol()
{
  syslog(LOG_INFO, "Destroying Symbol instance");
}

