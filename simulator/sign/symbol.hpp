/**
//==============================================================================
//                                symbol.hpp
//------------------------------------------------------------------------------
// @file symbol.hpp
//------------------------------------------------------------------------------
// @brief Header file containing definitions for class Symbol
//==============================================================================
*/

#pragma once

#include <unistd.h>

#include <string>
#include <array>

#define PIXEL_HEIGHT (7)

class Symbol
{
public:
  Symbol();
  ~Symbol();

  Symbol& SetASCII(char ASCII)
  {
    this->m_ASCII = ASCII;
    return *this;
  }

  Symbol& SetID(const uint8_t& ID)
  {
    this->m_ID = ID;
    return *this;
  }

  Symbol& SetWidth(const uint8_t& width)
  {
    this->m_width = width;
    return *this;
  }
  
  Symbol& SetSprite(const std::array<std::string, PIXEL_HEIGHT>& sprite)
  {
    this->m_sprite = sprite;
    return *this;
  }

  char GetASCII() const
  {
    return m_ASCII;
  }

  const uint8_t& GetID() const
  {
    return m_ID;
  }

  const uint8_t& GetWidth() const
  {
    return m_width;
  }

  const std::array<std::string, PIXEL_HEIGHT>& GetSprite() const
  {
    return m_sprite;
  }

private:
  uint8_t  m_ID;
  char     m_ASCII;
  uint8_t  m_width;

  std::array<std::string, PIXEL_HEIGHT>  m_sprite;
};
